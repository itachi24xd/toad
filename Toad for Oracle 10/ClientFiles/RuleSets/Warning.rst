<RULESET title="Warning" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6780145486</CREATED>
<MODIFIED>38447.6781896875</MODIFIED>
<COMMENTS>Rules in this set identify issues that are not a problem currently, but could become a problem if not addressed.</COMMENTS>
<RULESET_TOTAL>26</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="1" ico="298" total="26"/>
</SEVERITY>
<CATEGORY>
<CAT n="26" total="4"/>
<CAT n="30" total="1"/>
<CAT n="40" total="1"/>
<CAT n="44" total="3"/>
<CAT n="48" total="1"/>
<CAT n="52" total="1"/>
<CAT n="58" total="2"/>
<CAT n="60" total="11"/>
<CAT n="64" total="2"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="18"/>
<TYPE n="1" total="1"/>
<TYPE n="2" total="1"/>
<TYPE n="3" total="3"/>
<TYPE n="4" total="3"/>
</TYPES>
<SORT_ORDER>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2603"/>
<RULE rid="2606"/>
<RULE rid="2607"/>
<RULE rid="2608"/>
<RULE rid="5801"/>
<RULE rid="5813"/>
<RULE rid="6001"/>
<RULE rid="6002"/>
<RULE rid="6003"/>
<RULE rid="6411"/>
<RULE rid="6413"/>
<RULE rid="4002"/>
<RULE rid="4407"/>
<RULE rid="4408"/>
<RULE rid="4409"/>
<RULE rid="4808"/>
<RULE rid="6004"/>
<RULE rid="6005"/>
<RULE rid="6006"/>
<RULE rid="6007"/>
<RULE rid="6009"/>
<RULE rid="6010"/>
<RULE rid="6011"/>
<RULE rid="6013"/>
<RULE rid="3015"/>
<RULE rid="6415"/>
</RULES>
</RULESET>
