<RULESET title="Code Correctness" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6739052778</CREATED>
<MODIFIED>38447.6740753241</MODIFIED>
<COMMENTS>This rule set helps locate areas in your code that have a higher likelihood of error.</COMMENTS>
<RULESET_TOTAL>37</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="13"/>
<SEV n="1" ico="298" total="18"/>
<SEV n="2" ico="299" total="6"/>
</SEVERITY>
<CATEGORY>
<CAT n="26" total="5"/>
<CAT n="28" total="6"/>
<CAT n="42" total="1"/>
<CAT n="44" total="1"/>
<CAT n="46" total="1"/>
<CAT n="48" total="2"/>
<CAT n="50" total="2"/>
<CAT n="54" total="2"/>
<CAT n="58" total="2"/>
<CAT n="60" total="13"/>
<CAT n="64" total="1"/>
<CAT n="68" total="1"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="37"/>
</TYPES>
<SORT_ORDER>
<SORT>TYPES</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2601"/>
<RULE rid="2603"/>
<RULE rid="2606"/>
<RULE rid="2607"/>
<RULE rid="2608"/>
<RULE rid="2802"/>
<RULE rid="2801"/>
<RULE rid="2803"/>
<RULE rid="2804"/>
<RULE rid="2805"/>
<RULE rid="2827"/>
<RULE rid="4201"/>
<RULE rid="4401"/>
<RULE rid="4601"/>
<RULE rid="5001"/>
<RULE rid="5002"/>
<RULE rid="5401"/>
<RULE rid="5402"/>
<RULE rid="6801"/>
<RULE rid="5801"/>
<RULE rid="5813"/>
<RULE rid="6001"/>
<RULE rid="6002"/>
<RULE rid="6003"/>
<RULE rid="6401"/>
<RULE rid="4810"/>
<RULE rid="6013"/>
<RULE rid="6011"/>
<RULE rid="6010"/>
<RULE rid="6009"/>
<RULE rid="6007"/>
<RULE rid="6006"/>
<RULE rid="6005"/>
<RULE rid="6004"/>
<RULE rid="4808"/>
<RULE rid="6052"/>
<RULE rid="6051"/>
</RULES>
</RULESET>
