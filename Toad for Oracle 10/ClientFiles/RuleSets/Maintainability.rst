<RULESET title="Maintainability" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6755687963</CREATED>
<MODIFIED>38447.6757541435</MODIFIED>
<COMMENTS>This rule set helps identify areas of your code that could become problems for someone else maintaining the code. These tips will also help you to avoid data loss and conflicts.</COMMENTS>
<RULESET_TOTAL>27</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="26"/>
<SEV n="1" ico="298" total="1"/>
</SEVERITY>
<CATEGORY>
<CAT n="26" total="1"/>
<CAT n="28" total="1"/>
<CAT n="30" total="3"/>
<CAT n="46" total="1"/>
<CAT n="48" total="1"/>
<CAT n="58" total="2"/>
<CAT n="60" total="13"/>
<CAT n="64" total="3"/>
<CAT n="68" total="2"/>
</CATEGORY>
<TYPES>
<TYPE n="1" total="27"/>
</TYPES>
<SORT_ORDER>
<SORT>TYPES</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2610"/>
<RULE rid="2812"/>
<RULE rid="3001"/>
<RULE rid="3002"/>
<RULE rid="3003"/>
<RULE rid="4602"/>
<RULE rid="4807"/>
<RULE rid="6802"/>
<RULE rid="5803"/>
<RULE rid="5810"/>
<RULE rid="6413"/>
<RULE rid="6404"/>
<RULE rid="6414"/>
<RULE rid="6808"/>
<RULE rid="6045"/>
<RULE rid="6042"/>
<RULE rid="6024"/>
<RULE rid="6023"/>
<RULE rid="6022"/>
<RULE rid="6021"/>
<RULE rid="6020"/>
<RULE rid="6019"/>
<RULE rid="6018"/>
<RULE rid="6017"/>
<RULE rid="6016"/>
<RULE rid="6015"/>
<RULE rid="6014"/>
</RULES>
</RULESET>
