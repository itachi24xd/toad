<RULESET title="All Rules by Severity" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6723220833</CREATED>
<MODIFIED>38447.6727115625</MODIFIED>
<COMMENTS>This rule set tests your code against all rules in the Code Health Check universe including Code Correctness, Code Efficiency, Maintainability, Program Structure, and Readability.</COMMENTS>
<RULESET_TOTAL>154</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="121"/>
<SEV n="1" ico="298" total="26"/>
<SEV n="2" ico="299" total="7"/>
</SEVERITY>
<CATEGORY>
<CAT n="20" total="2"/>
<CAT n="24" total="1"/>
<CAT n="26" total="7"/>
<CAT n="28" total="10"/>
<CAT n="30" total="12"/>
<CAT n="38" total="4"/>
<CAT n="40" total="3"/>
<CAT n="42" total="5"/>
<CAT n="44" total="9"/>
<CAT n="46" total="3"/>
<CAT n="48" total="8"/>
<CAT n="50" total="4"/>
<CAT n="52" total="3"/>
<CAT n="54" total="4"/>
<CAT n="56" total="3"/>
<CAT n="58" total="12"/>
<CAT n="60" total="51"/>
<CAT n="62" total="1"/>
<CAT n="64" total="8"/>
<CAT n="68" total="4"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="37"/>
<TYPE n="1" total="27"/>
<TYPE n="2" total="15"/>
<TYPE n="3" total="34"/>
<TYPE n="4" total="41"/>
</TYPES>
<SORT_ORDER>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2601"/>
<RULE rid="2802"/>
<RULE rid="4201"/>
<RULE rid="4401"/>
<RULE rid="6401"/>
<RULE rid="2603"/>
<RULE rid="2606"/>
<RULE rid="2607"/>
<RULE rid="2608"/>
<RULE rid="4002"/>
<RULE rid="4407"/>
<RULE rid="4408"/>
<RULE rid="4409"/>
<RULE rid="5801"/>
<RULE rid="5813"/>
<RULE rid="6001"/>
<RULE rid="6002"/>
<RULE rid="6003"/>
<RULE rid="6411"/>
<RULE rid="6413"/>
<RULE rid="2006"/>
<RULE rid="2401"/>
<RULE rid="2610"/>
<RULE rid="2613"/>
<RULE rid="2801"/>
<RULE rid="2803"/>
<RULE rid="2804"/>
<RULE rid="2805"/>
<RULE rid="2827"/>
<RULE rid="2829"/>
<RULE rid="2830"/>
<RULE rid="2831"/>
<RULE rid="2812"/>
<RULE rid="3001"/>
<RULE rid="3002"/>
<RULE rid="3003"/>
<RULE rid="3004"/>
<RULE rid="3005"/>
<RULE rid="3006"/>
<RULE rid="3007"/>
<RULE rid="3010"/>
<RULE rid="3801"/>
<RULE rid="3803"/>
<RULE rid="3805"/>
<RULE rid="3806"/>
<RULE rid="4001"/>
<RULE rid="4003"/>
<RULE rid="4207"/>
<RULE rid="4204"/>
<RULE rid="4210"/>
<RULE rid="4213"/>
<RULE rid="4402"/>
<RULE rid="4403"/>
<RULE rid="4404"/>
<RULE rid="4405"/>
<RULE rid="4406"/>
<RULE rid="4601"/>
<RULE rid="4603"/>
<RULE rid="4602"/>
<RULE rid="4807"/>
<RULE rid="4804"/>
<RULE rid="4806"/>
<RULE rid="4801"/>
<RULE rid="4802"/>
<RULE rid="5001"/>
<RULE rid="5002"/>
<RULE rid="5004"/>
<RULE rid="5003"/>
<RULE rid="5201"/>
<RULE rid="5202"/>
<RULE rid="5401"/>
<RULE rid="5402"/>
<RULE rid="5405"/>
<RULE rid="5406"/>
<RULE rid="6801"/>
<RULE rid="6806"/>
<RULE rid="6802"/>
<RULE rid="5601"/>
<RULE rid="5602"/>
<RULE rid="5603"/>
<RULE rid="5804"/>
<RULE rid="5805"/>
<RULE rid="5806"/>
<RULE rid="5807"/>
<RULE rid="5808"/>
<RULE rid="5803"/>
<RULE rid="5810"/>
<RULE rid="5809"/>
<RULE rid="5814"/>
<RULE rid="5816"/>
<RULE rid="6202"/>
<RULE rid="6404"/>
<RULE rid="6414"/>
<RULE rid="6405"/>
<RULE rid="6406"/>
<RULE rid="6407"/>
<RULE rid="4810"/>
<RULE rid="6040"/>
<RULE rid="4808"/>
<RULE rid="6004"/>
<RULE rid="6005"/>
<RULE rid="6006"/>
<RULE rid="6007"/>
<RULE rid="6009"/>
<RULE rid="6010"/>
<RULE rid="6011"/>
<RULE rid="6013"/>
<RULE rid="3015"/>
<RULE rid="6415"/>
<RULE rid="6051"/>
<RULE rid="6052"/>
<RULE rid="4809"/>
<RULE rid="6043"/>
<RULE rid="6014"/>
<RULE rid="6015"/>
<RULE rid="6016"/>
<RULE rid="6017"/>
<RULE rid="6018"/>
<RULE rid="6019"/>
<RULE rid="6020"/>
<RULE rid="6021"/>
<RULE rid="6022"/>
<RULE rid="6023"/>
<RULE rid="6024"/>
<RULE rid="6042"/>
<RULE rid="6045"/>
<RULE rid="6808"/>
<RULE rid="3014"/>
<RULE rid="6030"/>
<RULE rid="6031"/>
<RULE rid="6032"/>
<RULE rid="6033"/>
<RULE rid="6034"/>
<RULE rid="6035"/>
<RULE rid="6036"/>
<RULE rid="6037"/>
<RULE rid="6038"/>
<RULE rid="6039"/>
<RULE rid="6041"/>
<RULE rid="2007"/>
<RULE rid="3013"/>
<RULE rid="3017"/>
<RULE rid="6025"/>
<RULE rid="6026"/>
<RULE rid="6027"/>
<RULE rid="6028"/>
<RULE rid="6029"/>
<RULE rid="6044"/>
<RULE rid="6046"/>
<RULE rid="6047"/>
<RULE rid="6048"/>
<RULE rid="6049"/>
<RULE rid="6050"/>
<RULE rid="6053"/>
</RULES>
</RULESET>
