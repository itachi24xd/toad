<RULESET title="Variables and Data Structures" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38069.5512658449</CREATED>
<MODIFIED>38071.6554581944</MODIFIED>
<COMMENTS>Rules in this set identify problems in the declaration or use of your program's variables and data structures.</COMMENTS>
<RULESET_TOTAL>37</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="0" total="29"/>
<SEV n="1" ico="1" total="5"/>
<SEV n="2" ico="2" total="3"/>
</SEVERITY>
<CATEGORY>
<CAT n="26" total="2"/>
<CAT n="28" total="13"/>
<CAT n="38" total="2"/>
<CAT n="50" total="2"/>
<CAT n="52" total="2"/>
<CAT n="58" total="3"/>
<CAT n="64" total="13"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="11"/>
<TYPE n="1" total="9"/>
<TYPE n="2" total="6"/>
<TYPE n="3" total="3"/>
<TYPE n="4" total="8"/>
</TYPES>
<SORT_ORDER>
<SORT>TYPES</SORT>
<SORT>CATEGORY</SORT>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
</PROPERTIES>
<RULES>
<RULE rid="6401"/>
<RULE rid="6402"/>
<RULE rid="6411"/>
<RULE rid="6413"/>
<RULE rid="6403"/>
<RULE rid="6404"/>
<RULE rid="6412"/>
<RULE rid="6414"/>
<RULE rid="6405"/>
<RULE rid="6406"/>
<RULE rid="6407"/>
<RULE rid="6408"/>
<RULE rid="6410"/>
<RULE rid="2609"/>
<RULE rid="2614"/>
<RULE rid="2802"/>
<RULE rid="2828"/>
<RULE rid="2801"/>
<RULE rid="2803"/>
<RULE rid="2804"/>
<RULE rid="2805"/>
<RULE rid="2827"/>
<RULE rid="2823"/>
<RULE rid="2829"/>
<RULE rid="2830"/>
<RULE rid="2831"/>
<RULE rid="2812"/>
<RULE rid="2810"/>
<RULE rid="3805"/>
<RULE rid="3806"/>
<RULE rid="5002"/>
<RULE rid="5003"/>
<RULE rid="5206"/>
<RULE rid="5202"/>
<RULE rid="5818"/>
<RULE rid="5802"/>
<RULE rid="5803"/>
</RULES>
</RULESET>
