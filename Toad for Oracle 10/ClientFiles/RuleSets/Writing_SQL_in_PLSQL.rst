<RULESET title="Writing SQL in PL/SQL" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38064.701991169</CREATED>
<MODIFIED>38071.6607594907</MODIFIED>
<COMMENTS>Rules in this set identify methods to improve the use  of SQL DML statements in your PL/SQL application.</COMMENTS>
<RULESET_TOTAL>21</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="0" total="15"/>
<SEV n="1" ico="1" total="6"/>
</SEVERITY>
<CATEGORY>
<CAT n="58" total="21"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="6"/>
<TYPE n="1" total="3"/>
<TYPE n="2" total="9"/>
<TYPE n="3" total="3"/>
</TYPES>
<SORT_ORDER>
<SORT>CATEGORY</SORT>
<SORT>TYPES</SORT>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
</PROPERTIES>
<RULES>
<RULE rid="5801"/>
<RULE rid="5811"/>
<RULE rid="5813"/>
<RULE rid="5819"/>
<RULE rid="5820"/>
<RULE rid="5821"/>
<RULE rid="5804"/>
<RULE rid="5805"/>
<RULE rid="5806"/>
<RULE rid="5807"/>
<RULE rid="5808"/>
<RULE rid="5812"/>
<RULE rid="5815"/>
<RULE rid="5817"/>
<RULE rid="5818"/>
<RULE rid="5802"/>
<RULE rid="5803"/>
<RULE rid="5810"/>
<RULE rid="5809"/>
<RULE rid="5814"/>
<RULE rid="5816"/>
</RULES>
</RULESET>
