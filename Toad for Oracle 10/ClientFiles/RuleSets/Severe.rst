<RULESET title="Severe" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6767106481</CREATED>
<MODIFIED>38447.6768568171</MODIFIED>
<COMMENTS>Rules in this set identify issues that are of a serious nature for the application now.</COMMENTS>
<RULESET_TOTAL>7</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="2" ico="299" total="7"/>
</SEVERITY>
<CATEGORY>
<CAT n="26" total="1"/>
<CAT n="28" total="1"/>
<CAT n="42" total="1"/>
<CAT n="44" total="1"/>
<CAT n="48" total="1"/>
<CAT n="60" total="1"/>
<CAT n="64" total="1"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="6"/>
<TYPE n="4" total="1"/>
</TYPES>
<SORT_ORDER>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2601"/>
<RULE rid="2802"/>
<RULE rid="4201"/>
<RULE rid="4401"/>
<RULE rid="6401"/>
<RULE rid="4810"/>
<RULE rid="6040"/>
</RULES>
</RULESET>
