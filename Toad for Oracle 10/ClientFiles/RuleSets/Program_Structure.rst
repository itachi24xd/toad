<RULESET title="Program Structure" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6759019329</CREATED>
<MODIFIED>38447.6760691898</MODIFIED>
<COMMENTS>This rule set supplies insights and suggestions for restructuring code, identifying redundancy and obsolete items, and locating inconsistencies.</COMMENTS>
<RULESET_TOTAL>41</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="37"/>
<SEV n="1" ico="298" total="3"/>
<SEV n="2" ico="299" total="1"/>
</SEVERITY>
<CATEGORY>
<CAT n="20" total="1"/>
<CAT n="24" total="1"/>
<CAT n="30" total="6"/>
<CAT n="38" total="4"/>
<CAT n="40" total="3"/>
<CAT n="48" total="2"/>
<CAT n="50" total="1"/>
<CAT n="52" total="3"/>
<CAT n="54" total="2"/>
<CAT n="56" total="3"/>
<CAT n="60" total="12"/>
<CAT n="64" total="3"/>
</CATEGORY>
<TYPES>
<TYPE n="4" total="41"/>
</TYPES>
<SORT_ORDER>
<SORT>TYPES</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2006"/>
<RULE rid="2401"/>
<RULE rid="3004"/>
<RULE rid="3005"/>
<RULE rid="3006"/>
<RULE rid="3007"/>
<RULE rid="3801"/>
<RULE rid="3803"/>
<RULE rid="3805"/>
<RULE rid="3806"/>
<RULE rid="4002"/>
<RULE rid="4001"/>
<RULE rid="4003"/>
<RULE rid="4804"/>
<RULE rid="4806"/>
<RULE rid="5004"/>
<RULE rid="5201"/>
<RULE rid="5202"/>
<RULE rid="5405"/>
<RULE rid="5406"/>
<RULE rid="5601"/>
<RULE rid="5602"/>
<RULE rid="5603"/>
<RULE rid="6405"/>
<RULE rid="6406"/>
<RULE rid="6407"/>
<RULE rid="6040"/>
<RULE rid="6415"/>
<RULE rid="3015"/>
<RULE rid="6041"/>
<RULE rid="6039"/>
<RULE rid="6038"/>
<RULE rid="6037"/>
<RULE rid="6036"/>
<RULE rid="6035"/>
<RULE rid="6034"/>
<RULE rid="6033"/>
<RULE rid="6032"/>
<RULE rid="6031"/>
<RULE rid="6030"/>
<RULE rid="3014"/>
</RULES>
</RULESET>
