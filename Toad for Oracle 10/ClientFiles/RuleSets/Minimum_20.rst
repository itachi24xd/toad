<RULESET  title="Top 20" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>39547.3996166319</CREATED>
<MODIFIED>39547.402571088</MODIFIED>
<COMMENTS>This rule set tests your code against a core subset of rules in the Code Health Check universe including Code Correctness, Code Efficiency, Maintainability, Program Structure, and Readability.</COMMENTS>
<RULESET_TOTAL>20</RULESET_TOTAL>
</SUMMARY>
<SEVERITY >
<SEV  n="0" ico="297" total="19"/>
<SEV  n="1" ico="298" total="1"/>
</SEVERITY>
<CATEGORY >
<CAT  n="30" total="1"/>
<CAT  n="38" total="3"/>
<CAT  n="40" total="1"/>
<CAT  n="42" total="1"/>
<CAT  n="46" total="1"/>
<CAT  n="50" total="2"/>
<CAT  n="54" total="2"/>
<CAT  n="56" total="1"/>
<CAT  n="58" total="4"/>
<CAT  n="62" total="1"/>
<CAT  n="64" total="2"/>
<CAT  n="68" total="1"/>
</CATEGORY>
<TYPES >
<TYPE  n="0" total="6"/>
<TYPE  n="2" total="4"/>
<TYPE  n="3" total="2"/>
<TYPE  n="4" total="8"/>
</TYPES>
<SORT_ORDER >
<SORT>TYPES</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS  xpert="1">
<Preferences >
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting >
<TableAccessed  IncludeDual="False" From="2" To="3"/>
<FullIndexScan  Selected="True"/>
</ComplexSetting>
<ProblematicSetting >
<FullTableScan  Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop  Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize  SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS >
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES >
<RULE  rid="5801"/>
<RULE  rid="5001"/>
<RULE  rid="5002"/>
<RULE  rid="5401"/>
<RULE  rid="5402"/>
<RULE  rid="6801"/>
<RULE  rid="4603"/>
<RULE  rid="5805"/>
<RULE  rid="5807"/>
<RULE  rid="6202"/>
<RULE  rid="3005"/>
<RULE  rid="3801"/>
<RULE  rid="3803"/>
<RULE  rid="3806"/>
<RULE  rid="4003"/>
<RULE  rid="6405"/>
<RULE  rid="6406"/>
<RULE  rid="4213"/>
<RULE  rid="5816"/>
<RULE  rid="5603"/>
</RULES>
</RULESET>
