<RULESET title="Code Efficiency" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6743156134</CREATED>
<MODIFIED>38447.6745396875</MODIFIED>
<COMMENTS>This rule set helps identify constructs that generally have a high impact on code size and run-time speed. Software efficiency problems can often be isolated to constructs that either generate large amounts of code or execute more slowly.</COMMENTS>
<RULESET_TOTAL>15</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="14"/>
<SEV n="1" ico="298" total="1"/>
</SEVERITY>
<CATEGORY>
<CAT n="28" total="3"/>
<CAT n="42" total="1"/>
<CAT n="46" total="1"/>
<CAT n="48" total="1"/>
<CAT n="58" total="5"/>
<CAT n="60" total="1"/>
<CAT n="62" total="1"/>
<CAT n="64" total="1"/>
<CAT n="68" total="1"/>
</CATEGORY>
<TYPES>
<TYPE n="2" total="15"/>
</TYPES>
<SORT_ORDER>
<SORT>TYPES</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="2829"/>
<RULE rid="2830"/>
<RULE rid="2831"/>
<RULE rid="4207"/>
<RULE rid="4603"/>
<RULE rid="6806"/>
<RULE rid="5804"/>
<RULE rid="5805"/>
<RULE rid="5806"/>
<RULE rid="5807"/>
<RULE rid="5808"/>
<RULE rid="6202"/>
<RULE rid="6411"/>
<RULE rid="6043"/>
<RULE rid="4809"/>
</RULES>
</RULESET>
