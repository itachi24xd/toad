<RULESET title="Readability" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6762668287</CREATED>
<MODIFIED>38447.6765959028</MODIFIED>
<COMMENTS>Rules in this category will help you identify areas in your code that violate generally accepted programming and readability criteria, or check for program consistency in naming and interface definition.</COMMENTS>
<RULESET_TOTAL>34</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="31"/>
<SEV n="1" ico="298" total="3"/>
</SEVERITY>
<CATEGORY>
<CAT n="20" total="1"/>
<CAT n="26" total="1"/>
<CAT n="30" total="3"/>
<CAT n="42" total="3"/>
<CAT n="44" total="8"/>
<CAT n="48" total="2"/>
<CAT n="50" total="1"/>
<CAT n="58" total="3"/>
<CAT n="60" total="12"/>
</CATEGORY>
<TYPES>
<TYPE n="3" total="34"/>
</TYPES>
<SORT_ORDER>
<SORT>TYPES</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="4802"/>
<RULE rid="4402"/>
<RULE rid="4403"/>
<RULE rid="4404"/>
<RULE rid="4405"/>
<RULE rid="4406"/>
<RULE rid="4204"/>
<RULE rid="4210"/>
<RULE rid="4213"/>
<RULE rid="5003"/>
<RULE rid="5809"/>
<RULE rid="5816"/>
<RULE rid="2613"/>
<RULE rid="3010"/>
<RULE rid="4407"/>
<RULE rid="4408"/>
<RULE rid="4409"/>
<RULE rid="4801"/>
<RULE rid="5814"/>
<RULE rid="6053"/>
<RULE rid="6050"/>
<RULE rid="6049"/>
<RULE rid="6048"/>
<RULE rid="6047"/>
<RULE rid="6046"/>
<RULE rid="6044"/>
<RULE rid="6029"/>
<RULE rid="6028"/>
<RULE rid="6027"/>
<RULE rid="6026"/>
<RULE rid="6025"/>
<RULE rid="3017"/>
<RULE rid="3013"/>
<RULE rid="2007"/>
</RULES>
</RULESET>
