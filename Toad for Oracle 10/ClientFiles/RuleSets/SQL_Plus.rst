<RULESET title="SQL*Plus Best Practices" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38447.6800555208</CREATED>
<MODIFIED>38447.6803373032</MODIFIED>
<COMMENTS>Rules in this set identify methods to improve your SQL*Plus scripting.</COMMENTS>
<RULESET_TOTAL>51</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="297" total="39"/>
<SEV n="1" ico="298" total="11"/>
<SEV n="2" ico="299" total="1"/>
</SEVERITY>
<CATEGORY>
<CAT n="60" total="51"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="13"/>
<TYPE n="1" total="13"/>
<TYPE n="2" total="1"/>
<TYPE n="3" total="12"/>
<TYPE n="4" total="12"/>
</TYPES>
<SORT_ORDER>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
<SQL_SCAN_OPTIONS run="1" xpert="1">
<Preferences>
<SkipComment>False</SkipComment>
<SkipDualTableSQL>False</SkipDualTableSQL>
<EliminateDuplicate>False</EliminateDuplicate>
<WholeWordMatch>False</WholeWordMatch>
<MaxWordSize>1024</MaxWordSize>
<SkipLineStart>0</SkipLineStart>
<ComplexSetting>
<TableAccessed IncludeDual="False" From="2" To="3"/>
<FullIndexScan Selected="True"/>
</ComplexSetting>
<ProblematicSetting>
<FullTableScan Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<NestedLoop Selected="True" IncludeDual="False" SizeIn="Kbytes" Size="8"/>
<RetrieveTableSize SelectIn="DBASegments"/>
</ProblematicSetting>
</Preferences>
</SQL_SCAN_OPTIONS>
<CODEXPERT_PREFS>
<OPTION1>0</OPTION1>
</CODEXPERT_PREFS>
</PROPERTIES>
<RULES>
<RULE rid="6040"/>
<RULE rid="6001"/>
<RULE rid="6002"/>
<RULE rid="6003"/>
<RULE rid="6004"/>
<RULE rid="6005"/>
<RULE rid="6006"/>
<RULE rid="6007"/>
<RULE rid="6009"/>
<RULE rid="6010"/>
<RULE rid="6011"/>
<RULE rid="6013"/>
<RULE rid="6051"/>
<RULE rid="6052"/>
<RULE rid="6043"/>
<RULE rid="6014"/>
<RULE rid="6015"/>
<RULE rid="6016"/>
<RULE rid="6017"/>
<RULE rid="6018"/>
<RULE rid="6019"/>
<RULE rid="6020"/>
<RULE rid="6021"/>
<RULE rid="6022"/>
<RULE rid="6023"/>
<RULE rid="6024"/>
<RULE rid="6042"/>
<RULE rid="6045"/>
<RULE rid="6030"/>
<RULE rid="6031"/>
<RULE rid="6032"/>
<RULE rid="6033"/>
<RULE rid="6034"/>
<RULE rid="6035"/>
<RULE rid="6036"/>
<RULE rid="6037"/>
<RULE rid="6038"/>
<RULE rid="6039"/>
<RULE rid="6041"/>
<RULE rid="6025"/>
<RULE rid="6026"/>
<RULE rid="6027"/>
<RULE rid="6028"/>
<RULE rid="6029"/>
<RULE rid="6044"/>
<RULE rid="6046"/>
<RULE rid="6047"/>
<RULE rid="6048"/>
<RULE rid="6049"/>
<RULE rid="6050"/>
<RULE rid="6053"/>
</RULES>
</RULESET>
