<RULESET title="Control Structures" version="NeedToAdd">
<PROPERTIES>
<SUMMARY>
<RULESET_TYPE>Quest</RULESET_TYPE>
<AUTHOR>Quest Software</AUTHOR>
<CREATED>38069.5600010185</CREATED>
<MODIFIED>38069.5702000694</MODIFIED>
<COMMENTS>This rule set helps isolate problems with conditional control processing such as loops, IF statements, and GOTO statements.</COMMENTS>
<RULESET_TOTAL>21</RULESET_TOTAL>
</SUMMARY>
<SEVERITY>
<SEV n="0" ico="0" total="17"/>
<SEV n="1" ico="1" total="3"/>
<SEV n="2" ico="2" total="1"/>
</SEVERITY>
<CATEGORY>
<CAT n="38" total="1"/>
<CAT n="40" total="4"/>
<CAT n="42" total="6"/>
<CAT n="44" total="3"/>
<CAT n="48" total="7"/>
</CATEGORY>
<TYPES>
<TYPE n="0" total="2"/>
<TYPE n="1" total="1"/>
<TYPE n="2" total="1"/>
<TYPE n="3" total="9"/>
<TYPE n="4" total="8"/>
</TYPES>
<SORT_ORDER>
<SORT>CATEGORY</SORT>
<SORT>TYPES</SORT>
<SORT>SEVERITY</SORT>
</SORT_ORDER>
</PROPERTIES>
<RULES>
<RULE rid="4807"/>
<RULE rid="4803"/>
<RULE rid="4804"/>
<RULE rid="4805"/>
<RULE rid="4806"/>
<RULE rid="4801"/>
<RULE rid="4802"/>
<RULE rid="4004"/>
<RULE rid="4002"/>
<RULE rid="4001"/>
<RULE rid="4003"/>
<RULE rid="4201"/>
<RULE rid="4207"/>
<RULE rid="4202"/>
<RULE rid="4204"/>
<RULE rid="4210"/>
<RULE rid="4213"/>
<RULE rid="3803"/>
<RULE rid="4408"/>
<RULE rid="4402"/>
<RULE rid="4406"/>
</RULES>
</RULESET>
