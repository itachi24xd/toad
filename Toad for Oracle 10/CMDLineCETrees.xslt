<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>
	<xsl:template match="trees">
		<html>
			<head>
				<title>Tree Frame</title>
				<script type="text/javascript">
					var LastHighlightedElem;
					
					function Toggle(e, node)
					{
						for(var i=node.parentNode.childNodes.length-1; i&gt;=0; i--)
						{
							var NodeToFold = node.parentNode.childNodes[i];
							if (NodeToFold.nodeName=='UL')
								break;
						}

						if (NodeToFold)
						{
							if (NodeToFold.className=='Collapsed')
							{
								NodeToFold.className = 'Expanded';
								node.src='minus.bmp'
							}
							else
							{
								NodeToFold.className='Collapsed'
								node.src='plus.bmp'
							}
						}
						if (e)
							e.cancelBubble = true
						else	
							window.event.cancelBubble = true;
					}

					function ChangeTab(ATab)
					{
						var OtherFrame = window.parent.frames[0].document;
						var CurrTabID = OtherFrame.all.CurrTabID.value;
						if (CurrTabID != '')
						{
							document.getElementById(CurrTabID).style.backgroundColor = 'white';
							document.getElementById(CurrTabID).style.color = 'black';
						}
						OtherFrame.all.CurrTabID.value = ATab.id;
						ATab.style.backgroundColor = 'blue';
						ATab.style.color = 'white';

						var CurrRowID = OtherFrame.all.CurrRowID.value;
						if (!CurrRowID)
							CurrRowID = 'row1';						
						
						var CurrRow = OtherFrame.getElementById(CurrRowID);
						if (document.createEvent)
						{
							var Event = document.createEvent('MouseEvents');
							Event.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
							CurrRow.dispatchEvent(Event);
						}
						else
							CurrRow.fireEvent('onclick');
					}
					
					function HighlightLineOfText(ANode)
					{
						var Frame0 = window.parent.frames[0].document;
						var Frame2 = window.parent.frames[2].document;
						var CurrScriptID = Frame0.all.CurrScriptID.value;
						var ARegExpr = /\((\d+).*\)/i
						var result = ARegExpr.exec(ANode.innerHTML); 
						
						if (result.length)
						{
							var TempStr = '_line' + result[1];
							var ScriptLineElem = Frame2.getElementById(CurrScriptID + TempStr);	
							if (ScriptLineElem)
							{
								if (LastHighlightedElem)
									LastHighlightedElem.style.backgroundColor = 'white';

								LastHighlightedElem = ScriptLineElem;
								ScriptLineElem.style.backgroundColor = 'yellow';
								ScriptLineElem.scrollIntoView();
							}	
						}
					}
					
					function OnLoadEvent()
					{
						ChangeTab(Tab1);	
					}
				</script>
				<style type="text/css">
					
					ul.Tree {display: none; white-space: nowrap}
					span.NodeText {margin-left: 5px; position: relative; top: -3px; font-size: 8pt}
					span.HeadNodeTextNormal {color: black; margin-right: 5px; text-decoration: underline; font-weight: bold}
					span.HeadNodeTextStatic {color: blue; font-weight: bold}

					span.rule_typeCol0TextNormal {color: black}
					span.rule_typeCol0TextStatic {color: blue}

					span.ruleCol0TextNormal {color: black; margin-right: 5px}
					span.ruleCol0TextStatic {color: blue; margin-right: 60px}
					span.ruleCol1TextStatic {color: red}

					span.textCol0TextStatic {color: blue; margin-right: 60px}
					span.textCol1TextStatic {color: teal}

					span.propertyCol0TextNormal {color: black; margin-right: 5px}
					span.propertyCol0TextStatic {color: blue}

					span.textCol0TextStatic {color: blue; margin-right: 60px}
					span.textCol1TextStatic {color: teal}

					sql_scan_category_Col0TextNormal {color: black; margin-right: 5px}
					span.sql_scan_categoryCol0TextStatic {color: blue}
					span.dataCol0TextStatic {color: blue; margin-right: 60px}
					span.dataCol1TextStatic {color: teal}
					span.msgCol0TextStatic {color: red}

					th {cursor: pointer}
					#Tab1 {background-color: blue; color: white; font-size: 10pt}
					#Tab2 {font-size: 10pt}
					#Tab3 {font-size: 10pt}

					li { list-style: none; margin-left:-20px}
					li.TextLocationNode {cursor: pointer}
					li.Node {cursor: default}
					
					.Collapsed {display: none}
					.Expanded {display: block}

					table {border-color: gray black black gray}
					img {cursor: default}
				</style>
			</head>
			<body onLoad="OnLoadEvent()">
				<table border="5">
					<tr>
						<th id="Tab1" onClick="ChangeTab(this)">All Rules by Objects</th>
						<th id="Tab2" onClick="ChangeTab(this)">Properties</th>
						<th id="Tab3" onClick="ChangeTab(this)">SQL Scan</th>
					</tr>
				</table>
				<br />
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="object">
		<div class="object">
			<xsl:attribute name="id">object<xsl:value-of select="count(preceding::object)+1" /></xsl:attribute>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	<xsl:template match="all_rules_by_objects">
		<ul class="Tree">
			<xsl:attribute name="id">object<xsl:value-of select="count(preceding::object)+1" />Tree1</xsl:attribute>
			<li class="Node">
				<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				<span class="NodeText">
					<span class="HeadNodeTextNormal">
						<xsl:value-of select="@ruleset" />
					</span>
					<span class="HeadNodeTextStatic">
						<xsl:value-of select="@object" />
					</span>
				</span>
			</li>
			<xsl:apply-templates />
		</ul>
	</xsl:template>
	<xsl:template match="rule_type">
		<li class="Node">
			<xsl:choose>
				<xsl:when test="count(*)&gt;0">
					<img src="plus.bmp" alt="plus.bmp" class="NodeImage" onClick="Toggle(arguments[0],this)" />
				</xsl:when>
				<xsl:otherwise>
					<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@imageindex">
				<img>
					<xsl:attribute name="src"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
					<xsl:attribute name="alt"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
				</img>
			</xsl:if>
			<span class="NodeText">
				<span class="rule_typeCol0TextNormal">
					<xsl:value-of select="@name" />					
				</span>				    
				    <xsl:if test="@imageindex">
					<xsl:choose>
					    <xsl:when test="@name='Toad Code Rating(TCR)'">
						<span class="rule_typeCol0TextStatic"> &#160; <xsl:value-of select="./@rulesflagged" /> </span>
					    </xsl:when>
					    <xsl:otherwise>				    
						<span class="rule_typeCol0TextStatic"> (<xsl:value-of select="./@rulesflagged" /> Rules / <xsl:value-of select="./@occurences" /> Occurences)</span>
					    </xsl:otherwise>				    						
					</xsl:choose>
				    </xsl:if>
			</span>
			<xsl:if test="count(*)&gt;0">
				<ul class="Collapsed">
					<xsl:apply-templates />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="rule">
		<li class="Node">
			<xsl:choose>
				<xsl:when test="count(*)&gt;0">
					<img src="plus.bmp" alt="plus.bmp" class="NodeImage" onClick="Toggle(arguments[0],this)" />
				</xsl:when>
				<xsl:otherwise>
					<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				</xsl:otherwise>
			</xsl:choose>
			<img>
				<xsl:attribute name="src"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
				<xsl:attribute name="alt"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
			</img>
			<span class="NodeText">
				<span class="ruleCol0TextNormal"><xsl:value-of select="@category" /> - <xsl:value-of select="@code" /></span>
				<span class="ruleCol0TextStatic">(<xsl:value-of select="@occurences" /> Occurences)</span>
				<span class="ruleCol1TextStatic">
					<xsl:value-of select="@description" />
				</span>
			</span>
			<xsl:if test="count(*)&gt;0">
				<ul class="Collapsed">
					<xsl:apply-templates />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="rule_text">
		<li class="TextLocationNode" onClick="HighlightLineOfText(this)">
			<img src="square.bmp" alt="square.bmp" class="NodeImage" />
			<span class="NodeText">
				<span class="textCol0TextStatic">(<xsl:value-of select="@linenum" />, <xsl:value-of select="@linepos" />)</span>
				<span class="textCol1TextStatic">
					<xsl:value-of select="." />
				</span>
			</span>
		</li>
	</xsl:template>
	<xsl:template match="properties">
		<ul class="Tree">
			<xsl:attribute name="id">object<xsl:value-of select="count(preceding::object)+1" />Tree2</xsl:attribute>
			<xsl:apply-templates />
		</ul>
	</xsl:template>
	<xsl:template match="property_category">
		<li class="Node">
			<xsl:choose>
				<xsl:when test="count(*)&gt;0">
					<img src="plus.bmp" alt="plus.bmp" class="NodeImage" onClick="Toggle(arguments[0],this)" />
				</xsl:when>
				<xsl:otherwise>
					<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				</xsl:otherwise>
			</xsl:choose>
			<img>
				<xsl:attribute name="src"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
				<xsl:attribute name="alt"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
			</img>
			<span class="NodeText">
				<span class="property_categoryCol0TextNormal">
					<xsl:value-of select="@name" />
				</span>
			</span>
			<xsl:if test="count(*)&gt;0">
				<ul class="Collapsed">
					<xsl:apply-templates />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="property">
		<li class="Node">
			<xsl:choose>
				<xsl:when test="count(*)&gt;0">
					<img src="plus.bmp" alt="plus.bmp" class="NodeImage" onClick="Toggle(arguments[0],this)" />
				</xsl:when>
				<xsl:otherwise>
					<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				</xsl:otherwise>
			</xsl:choose>
			<span class="NodeText">
				<span class="propertyCol0TextNormal">
					<xsl:value-of select="@name" />
				</span>
				<span class="propertyCol0TextStatic">(<xsl:value-of select="@occurences" /> Occurences)</span>
			</span>
			<xsl:if test="count(*)&gt;0">
				<ul class="Collapsed">
					<xsl:apply-templates />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="property_text">
		<li class="TextLocationNode" onClick="HighlightLineOfText(this)">
			<img src="square.bmp" alt="square.bmp" class="NodeImage" />
			<span class="NodeText">
				<span class="textCol0TextStatic">(<xsl:value-of select="@linenum" />, <xsl:value-of select="@linepos" />)</span>
				<span class="textCol1TextStatic">
					<xsl:value-of select="." />
				</span>
			</span>
		</li>
	</xsl:template>
	<xsl:template match="sql_scan">
		<ul class="Tree">
			<xsl:attribute name="id">object<xsl:value-of select="count(preceding::object)+1" />Tree3</xsl:attribute>
			<xsl:apply-templates />
		</ul>
	</xsl:template>
	<xsl:template match="sql_scan_category">
		<li class="Node">
			<xsl:choose>
				<xsl:when test="count(*)&gt;0">
					<img src="plus.bmp" alt="plus.bmp" class="NodeImage" onClick="Toggle(arguments[0],this)" />
				</xsl:when>
				<xsl:otherwise>
					<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				</xsl:otherwise>
			</xsl:choose>
			<img>
				<xsl:attribute name="src"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
				<xsl:attribute name="alt"><xsl:value-of select="@imageindex" />.bmp</xsl:attribute>
			</img>
			<span class="NodeText">
				<span class="sql_scan_category_Col0TextNormal">
					<xsl:value-of select="@name" />
				</span>
				<span class="sql_scan_categoryCol0TextStatic">
					(<xsl:value-of select="@occurences" /> Occurrences)
				</span>
			</span>
			<xsl:if test="count(*)&gt;0">
				<ul class="Collapsed">
					<xsl:apply-templates />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="data">
		<li class="TextLocationNode" onClick="HighlightLineOfText(this)">
			<xsl:choose>
				<xsl:when test="count(*)&gt;0">
					<img src="plus.bmp" alt="plus.bmp" class="NodeImage" onClick="Toggle(arguments[0],this)" />
				</xsl:when>
				<xsl:otherwise>
					<img src="leaf.bmp" alt="leaf.bmp" class="NodeImage" />
				</xsl:otherwise>
			</xsl:choose>
			<span class="NodeText">
				<span class="dataCol0TextStatic">
					(<xsl:value-of select="@linenum" />, <xsl:value-of select="@linepos" />) 
				</span>
				<span class="dataCol1TextStatic">
					<xsl:value-of select="@text" />
				</span>
			</span>
			<xsl:if test="count(*)&gt;0">
				<ul class="Collapsed">
					<xsl:apply-templates />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="msg">
		<li class="Node">
			<img src="square.bmp" alt="square.bmp" class="NodeImage" />
			<span class="NodeText">
				<span class="msgCol0TextStatic">
					<xsl:value-of select="." />
				</span>
			</span>
		</li>
	</xsl:template>
</xsl:stylesheet>
