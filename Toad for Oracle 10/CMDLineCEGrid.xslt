<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>
	<xsl:template match="grid">
		<html>
			<head>
				<title>Grid Frame</title>
				<script type="text/javascript">
					function SelectRow(ARow)
					{
						var Frame1 = window.parent.frames[1].document;
						var Frame2 = window.parent.frames[2].document;

						if ((CurrRowID.value != '') <xsl:text disable-output-escaping="yes">&#38;&#38;</xsl:text> (document.getElementById(CurrRowID.value) != null))
						{
							document.getElementById(CurrRowID.value).style.backgroundColor = 'white';
							document.getElementById(CurrRowID.value).style.color = 'black';
						}

						ARow.style.backgroundColor = 'blue';
						ARow.style.color = 'white';

						CurrRowID.value = ARow.id;

						if ((CurrTreeID.value != '') <xsl:text disable-output-escaping="yes">&#38;&#38;</xsl:text> (Frame1.getElementById(CurrTreeID.value) != null))
						{
							Frame1.getElementById(CurrTreeID.value).style.display = 'none';
							Frame2.getElementById(CurrScriptID.value).style.display = 'none';
						}	

						CurrTreeID.value = 'object' + CurrRowID.value.match('row(\\d+)')[1];
						CurrScriptID.value = 'script' + CurrRowID.value.match('row(\\d+)')[1];

    					if (CurrTabID.value == 'Tab1')
    						CurrTreeID.value = CurrTreeID.value + 'Tree1'
    					else if (CurrTabID.value == 'Tab2')
    						CurrTreeID.value = CurrTreeID.value + 'Tree2'
    					else if (CurrTabID.value == "Tab3")
    						CurrTreeID.value = CurrTreeID.value + 'Tree3'

						Frame1.getElementById(CurrTreeID.value).style.display = 'block';
						Frame2.getElementById(CurrScriptID.value).style.display = 'block';
					}
					
				</script>
				<style type="text/css">
					th {background-color: silver; white-space: nowrap;}
					td {font-size: 8pt; text-align: center; cursor: pointer; white-space: nowrap}
					td.SuperHeader {font-weight: 900; font-size: 10pt; text-align: center}
					td.RedBorder {background-color: red; color: black}
					td.YellowBorder {background-color: yellow; color: black}
					td.GreenBorder {background-color: green; color: black}										
					td.OrangeBorder {background-color: orange; color: black}
					#date_ran {width: 100%; font-weight: 300; font-size: 8pt; text-align: right}
				</style>
			</head>
			<body>
				<input id="CurrRowID" type="hidden" />
				<input id="CurrTreeID" type="hidden" />
				<input id="CurrScriptID" type="hidden" />
				<input id="CurrTabID" type="hidden" value="Tab1" />
				<h2><img src="toad.bmp" alt="toad.bmp" />CodeXpert<span id="date_ran">
						<xsl:value-of select="@date_ran" />
					</span></h2>
				<table border="1">
					<tr>
						<th class="SuperHeader" colspan="5">File/Object Information</th>
						<th class="superheader" colspan="5">Totals by Objective</th>
						<th class="superheader" colspan="3">Totals by Severity</th>
						<th class="superheader" colspan="4">SQL Scan Summary</th>
						<th class="superheader" colspan="5">Code Metrics</th>
					</tr>
					<tr>
						<xsl:for-each select="summary_info[1]/*">
							<xsl:choose>
								<xsl:when test="local-name(.)='source'">
									<th>Source</th>
								</xsl:when>
								<xsl:when test="local-name(.)='owner'">
									<th>Owner</th>
								</xsl:when>
								<xsl:when test="local-name(.)='_type'">
									<th>Type</th>
								</xsl:when>
								<xsl:when test="local-name(.)='object_name'">
									<th>File/Object Name</th>
								</xsl:when>
								<xsl:when test="local-name(.)='connection'">
									<th>Connection</th>
								</xsl:when>
								<xsl:when test="local-name(.)='code_correctness_rules'">
									<th>Code Correctness</th>
								</xsl:when>
								<xsl:when test="local-name(.)='maintainability_rules'">
									<th>Maintainability</th>
								</xsl:when>
								<xsl:when test="local-name(.)='efficiency_rules'">
									<th>Efficiency</th>
								</xsl:when>
								<xsl:when test="local-name(.)='readability_rules'">
									<th>Readability</th>
								</xsl:when>
								<xsl:when test="local-name(.)='program_structure_rules'">
									<th>Program Structure</th>
								</xsl:when>
								<xsl:when test="local-name(.)='severe_rules'">
									<th>Severe</th>
								</xsl:when>
								<xsl:when test="local-name(.)='warning_rules'">
									<th>Warning</th>
								</xsl:when>
								<xsl:when test="local-name(.)='information_rules'">
									<th>Information</th>
								</xsl:when>
								<xsl:when test="local-name(.)='scan_problematic_rules'">
									<th>Problematic</th>
								</xsl:when>
								<xsl:when test="local-name(.)='scan_complex_rules'">
									<th>Complex</th>
								</xsl:when>
								<xsl:when test="local-name(.)='scan_simple_rules'">
									<th>Simple</th>
								</xsl:when>
								<xsl:when test="local-name(.)='scan_invalid_rules'">
									<th>Invalid</th>
								</xsl:when>								
								<xsl:when test="local-name(.)='num_statements'">
									<th># Statements</th>
								</xsl:when>
								<xsl:when test="local-name(.)='tcr'">
									<th>Toad Code Rating (TCR)</th>
								</xsl:when>																
								<xsl:when test="local-name(.)='halsted_metric'">
									<th>Halsted Volume</th>
								</xsl:when>
								<xsl:when test="local-name(.)='mccabe_cyclomatic'">
									<th>McCabe's Cyclomatic</th>
								</xsl:when>
								<xsl:when test="local-name(.)='maintainability_index'">
									<th>Maintainability Index</th>
								</xsl:when>								
							</xsl:choose>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="summary_info">
						<tr onClick="SelectRow(this)">
							<xsl:attribute name="id">row<xsl:value-of select="position()" /></xsl:attribute>
							<xsl:for-each select="*">
								<xsl:choose>
									<xsl:when test="local-name(.)='source'">
										<td>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='owner'">
										<td>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='_type'">
										<td>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='object_name'">
										<td>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='connection'">
										<td>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='code_correctness_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../code_correctness_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='maintainability_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../maintainability_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='efficiency_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../efficiency_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='readability_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../readability_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='program_structure_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../program_structure_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='severe_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../severe_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='warning_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../warning_occurrences" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='information_rules'">
										<td>
									<xsl:value-of select="." /> / <xsl:value-of select="../information_occurrences" />
								</td>
									</xsl:when>
									
									<xsl:when test="local-name(.)='scan_problematic_rules'">
										<td>
									 <xsl:value-of select="../problematic_rules" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='scan_complex_rules'">
										<td>
									<xsl:value-of select="../complex_rules" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='scan_simple_rules'">
										<td>
									<xsl:value-of select="../simple_rules" />
								</td>
									</xsl:when>
									<xsl:when test="local-name(.)='scan_invalid_rules'">
										<td>
									<xsl:value-of select="../invalid_rules" />
								</td>
									</xsl:when>
																		
									<xsl:when test="local-name(.)='num_statements'">
										<td>
											<xsl:attribute name="class">
												<xsl:call-template name="color_metrics_column_border">
													<xsl:with-param name="column_name" select="local-name(.)" />
													<xsl:with-param name="value" select="." />
												</xsl:call-template>
											</xsl:attribute>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='tcr'">
										<td>
											<xsl:attribute name="class">
												<xsl:call-template name="color_metrics_column_border">
													<xsl:with-param name="column_name" select="local-name(.)" />
													<xsl:with-param name="value" select="." />
												</xsl:call-template>
											</xsl:attribute>
											<xsl:value-of select="." />
										</td>
									</xsl:when>									
									<xsl:when test="local-name(.)='halsted_metric'">
										<td>
											<xsl:attribute name="class">
												<xsl:call-template name="color_metrics_column_border">
													<xsl:with-param name="column_name" select="local-name(.)" />
													<xsl:with-param name="value" select="." />
												</xsl:call-template>
											</xsl:attribute>
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='mccabe_cyclomatic'">
										<td>
											<xsl:attribute name="class">
												<xsl:call-template name="color_metrics_column_border">
													<xsl:with-param name="column_name" select="local-name(.)" />
													<xsl:with-param name="value" select="." />
												</xsl:call-template>
											</xsl:attribute>
										
											<xsl:value-of select="." />
										</td>
									</xsl:when>
									<xsl:when test="local-name(.)='maintainability_index'">
										<td>
											<xsl:attribute name="class">
												<xsl:call-template name="color_metrics_column_border">
													<xsl:with-param name="column_name" select="local-name(.)" />
													<xsl:with-param name="value" select="." />
												</xsl:call-template>
											</xsl:attribute>
											<xsl:value-of select="." />
										</td>
									</xsl:when>								
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="color_metrics_column_border">
		<xsl:param name="value" />
		<xsl:param name="column_name" />
		<xsl:choose>
			<xsl:when test="$column_name='num_statements'">
				<xsl:choose>
					<xsl:when test="$value>=251">
						<xsl:text>RedBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=101">
						<xsl:text>YellowBorder</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>WhiteBorder</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$column_name='tcr'">
				<xsl:choose>
					<xsl:when test="$value>=4">
						<xsl:text>RedBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=3">
						<xsl:text>OrangeBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=2">
						<xsl:text>YellowBorder</xsl:text>
					</xsl:when>					
					<xsl:otherwise>
						<xsl:text>GreenBorder</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>			
			<xsl:when test="$column_name='halsted_metric'">
				<xsl:choose>
					<xsl:when test="$value>=3001">
						<xsl:text>RedBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=1001">
						<xsl:text>YellowBorder</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>WhiteBorder</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$column_name='mccabe_cyclomatic'">
				<xsl:choose>
					<xsl:when test="$value>=51">
						<xsl:text>RedBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=21">
						<xsl:text>OrangeBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=11">
						<xsl:text>YellowBorder</xsl:text>
					</xsl:when>					
					<xsl:otherwise>
						<xsl:text>WhiteBorder</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$column_name='maintainability_index'">
				<xsl:choose>
					<xsl:when test="$value>=85">
						<xsl:text>WhiteBorder</xsl:text>
					</xsl:when>
					<xsl:when test="$value>=65">
						<xsl:text>YellowBorder</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>RedBorder</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>		
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
