<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>
	<xsl:template match="scripts">
		<html>
			<head>
				<title>Script Frame</title>
				<script type="text/javascript"></script>
				<style type="text/css">
					pre {display: inline; font-size: 8pt}
					span.line {width: 100%}
					div.script {display: none}
				</style>
			</head>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="script">
		<xsl:variable name="id">script<xsl:value-of select="count(preceding::script)+1" /></xsl:variable>
		<div class="script">
			<xsl:attribute name="id">
				<xsl:value-of select="$id" />
			</xsl:attribute>
			<xsl:for-each select="line">
				<span class="line">
					<xsl:attribute name="id"><xsl:value-of select="$id" />_line<xsl:value-of select="@linenum" /></xsl:attribute>
					<pre>
						<xsl:apply-templates />
					</pre>
				</span>
				<br />
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>
